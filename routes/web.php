<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('formSubmit','TemplatesController@uploadTemplate');
Route::get ('/gettemplates', 'TemplatesController@templatesList');
Route::get ('/getdocfields/{id}', 'TemplatesController@getDocFields');
Route::post ('/templates/{id}', 'TemplatesController@deleteItem' );
Route::post('/submit', 'TemplatesController@submit');
Route::get('/download/{file}', 'TemplatesController@download')->name('download');

