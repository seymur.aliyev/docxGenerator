<?php
/**
 * Created by PhpStorm.
 * User: Lenovo
 * Date: 07.05.2019
 * Time: 22:21
 */

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Templates extends Model {

	public static function getTemplates() {
		$sql = "SELECT * FROM templates";
		return DB::select(DB::raw($sql));
	}

	public static function getDocument($id) {
		if (empty($id)) { return false; }
		$sql = "SELECT filename FROM templates WHERE id=:id";
		return DB::select(DB::raw($sql), [':id' => $id]);
	}
}